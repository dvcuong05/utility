﻿
namespace NCUtils.Models
{
    public class MockupRequestData
    {
        string imageUri;
        string fileName;
        string saveToFolder;
        int width = 3600;
        int height = 4800;
        float dpix = 300;
        float dpiy = 300;
        string outputFolderGoogleDriveCode;

        public int Width { get => width; set => width = value; }
        public int Height { get => height; set => height = value; }
        public float Dpix { get => dpix; set => dpix = value; }
        public float Dpiy { get => dpiy; set => dpiy = value; }
        public string OutputFolderGoogleDriveCode { get => outputFolderGoogleDriveCode; set => outputFolderGoogleDriveCode = value; }
        public string ImageUri { get => imageUri; set => imageUri = value; }
        public string FileName { get => fileName; set => fileName = value; }
        public string SaveToFolder { get => saveToFolder; set => saveToFolder = value; }
    }
}