﻿
namespace NCUtils
{
    class LastChanges
    {
        string textBoxParentFolderDrive;
        string textBoxOutputDriveRename;
        string textBoxTrackingFilePath;

        string textBoxSheetId;
        string textBoxDBIP;
        string textBoxDBUser;
        string textBoxDBPass;
        string textBoxDBName;
        string textBoxSiteAddress;
        bool isBingAds = false;

        string textBoxFolderTeescape;

        public string TextBoxParentFolderDrive { get => textBoxParentFolderDrive; set => textBoxParentFolderDrive = value; }
        public string TextBoxOutputDriveRename { get => textBoxOutputDriveRename; set => textBoxOutputDriveRename = value; }
        public string TextBoxTrackingFilePath { get => textBoxTrackingFilePath; set => textBoxTrackingFilePath = value; }
        public string TextBoxSheetId { get => textBoxSheetId; set => textBoxSheetId = value; }
        public string TextBoxDBIP { get => textBoxDBIP; set => textBoxDBIP = value; }
        public string TextBoxDBUser { get => textBoxDBUser; set => textBoxDBUser = value; }
        public string TextBoxDBPass { get => textBoxDBPass; set => textBoxDBPass = value; }
        public string TextBoxDBName { get => textBoxDBName; set => textBoxDBName = value; }
        public string TextBoxSiteAddress { get => textBoxSiteAddress; set => textBoxSiteAddress = value; }
        public string TextBoxFolderTeescape { get => textBoxFolderTeescape; set => textBoxFolderTeescape = value; }
        public bool IsBingAds { get => isBingAds; set => isBingAds = value; }
    }
}
