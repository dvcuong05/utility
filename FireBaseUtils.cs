﻿namespace NCUtils
{
    using Main.classes.firebase;
    using NCUtils.DataClass.firebase.objects;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Net;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;

    public class FireBaseUtils
    {
        public static FirebaseDB firebaseDB;

        internal IDictionary<string, Design> dictionary = null;

        public FireBaseUtils(string firebaseUrl, string[] nodes)
        {
            try
            {
                firebaseDB = new FirebaseDB(firebaseUrl);//"https://ncsoftwareapp.firebaseio.com/"
                //firebaseDBStoreConfig = firebaseDB;//.Node("Teescape").Node("StoreConfig");
                foreach (string node in nodes)
                {
                    firebaseDB = firebaseDB.Node(node);
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Error load Firebase setting.Exception:" + ex.Message);
            }
        }

        public void InsertFirebaseRow(string nodeName, string data)
        {
            firebaseDB.NodePath(nodeName).Put(data);
        }

        public string findPictureName(string firebaseKey)
        {
            firebaseKey = buildKeyName(firebaseKey);
            try
            {
                if (dictionary != null)
                {
                    var design = dictionary[firebaseKey];
                    if (design != null)
                    {
                        dictionary.Remove(firebaseKey);
                        return design.Name;
                    }
                }
                // Referring to Node with name "Teams"  
                FirebaseDB firebaseDBTeams = firebaseDB;//.Node(firebaseKey);
                FirebaseResponse getResponse = firebaseDBTeams.Get();
                if (getResponse.Success)
                {
                    if (getResponse.JSONContent != "null")
                    {
                        dictionary = JsonConvert.DeserializeObject<Dictionary<string, Design>>(getResponse.JSONContent);
                        var design = dictionary[firebaseKey];
                        if(design != null)
                        {
                            dictionary.Remove(firebaseKey);
                           return design.Name;
                        }
                    }
                }
            }
            catch (System.Exception)
            {
                //MessageBox.Show("The store is not config important information. Please open setting panel and do it.", "Error");
            }
            return null;
        }

        public void downloadFileFromURL(string outputFolder, List<string> trackingFile)
        {
            if (dictionary != null)
            {
                foreach (var item in dictionary)
                {
                    var design = item.Value;
                    if (design != null && !trackingFile.Contains(design.Name))
                    {
                        using (WebClient client = new WebClient())
                        {
                            client.DownloadFile(new System.Uri(design.DriveLink), @outputFolder + "\\" + design.Name + ".png");
                            trackingFile.Add(design.Name);
                        }
                    }
                }
                dictionary.Clear();
            }
            
        }

        public static string buildKeyName(string str)
        {
            Regex rgx = new Regex(@"[^a-zA-Z0-9 \s+]");
            return rgx.Replace(str, "").Replace("+", "-").Replace(" ", "-");
        }
    }
}
