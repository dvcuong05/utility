﻿
namespace NCUtils
{
    using Google.Apis.Auth.OAuth2;
    using Google.Apis.Drive.v3;
    using Google.Apis.Drive.v3.Data;
    using Google.Apis.Services;
    using Google.Apis.Util.Store;
    using NCUtils.DataClass.firebase;
    using NCUtils.DataClass.firebase.objects;
    using NCUtils.Models;
    using NCUtils.Services;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public class GoogleDrive
    {
        string merchFolderId = "1Y6Y_3FVKXSJ2rcG7sZf1mkyfHiecZs76";
        string teescapeFolderId = "1HU8sFqH4AgKdSi-qYp48aHkij9kI2JT9";
        string teeSpringFolderId = "1uN5QKiU-jxMhhqJjYFZTiIlxEVGPvOWZ";
        Dictionary<string, string> ASSET_FOLDERS = new Dictionary<string, string>()
        {
            { "teescape", "13HnVM9ydajVnhgwB19HqpccuzXB2NjHw" },
            { "merch", "1mncjIsAZoycU_zfzTMLJh6qt51QNubH8" },
            { "teespring", "1pVgs1cMszJPqU-Tx0macSg7F8hKcujsx" }
        };

        private string CURRENT_APP_NAME = "teescape";

        internal static string[] Scopes = { DriveService.Scope.Drive };
        internal static string ApplicationName = "NCSoftware Google Drive";
        private string ggAuthenFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\assets\\configs\\";
        DriveService service;

        public GoogleDrive(string appName)
        {
            this.CURRENT_APP_NAME = appName;
        }
        private UserCredential GetCredentials()
        {
            try
            {
                UserCredential credential;
                string credentialsFile = ggAuthenFolder + "gappcache.bat";
                using (var stream = new FileStream(credentialsFile, FileMode.Open, FileAccess.Read))
                {
                    string credPath = ggAuthenFolder;
                    credPath = Path.Combine(credPath, "gappcache");
                    try
                    {
                        credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                            GoogleClientSecrets.Load(stream).Secrets,
                            Scopes,
                            "user",
                            CancellationToken.None,
                            new FileDataStore(credPath, true)).Result;
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
                return credential;
            }
            catch (Exception ex)
            {
                Console.WriteLine("could not get gappcahe file", ex.Message);
                return null;
            }
        }

        public void cleanAuthenFolder()
        {
            string credPath = ggAuthenFolder;
            credPath = Path.Combine(credPath, "gappcache");
            if (Directory.Exists(credPath))
            {
                Directory.Delete(credPath, true);
            }
        }

        public bool hasAuthentication()
        {
            string credPath = ggAuthenFolder;
            credPath = Path.Combine(credPath, "gappcache");
            if (Directory.Exists(credPath))
            {
                string[] files = Directory.GetFiles(credPath);
                return files.Length > 0;
            }
            return false;
        }

        private string getParentFolderId(string applicationName)
        {
            string parentId = merchFolderId;
            if (applicationName.ToLower() == "teescape")
            {
                parentId = teescapeFolderId;
            }
            else if (applicationName == "teespring")
            {
                parentId = teeSpringFolderId;
            }
            return parentId;
        }
        // Return folder id created by Google Drive
        public string createDriveFolder(string parentId, string folderName, string folderDesc, DriveService service)
        {
            string folderid;
            //get folder id by name
            var fileMetadatas = new Google.Apis.Drive.v3.Data.File()
            {
                Name = folderName,
                MimeType = "application/vnd.google-apps.folder",
                Description = folderDesc,

            };
            fileMetadatas.Parents = new List<string>
            {
              parentId
            };
            var requests = service.Files.Create(fileMetadatas);
            requests.Fields = "id";
            var files = requests.Execute();
            folderid = files.Id;
            /*try
            {
                Permission userPermission = new Permission();
                userPermission.Type = "user";
                userPermission.Role = "owner";
                userPermission.EmailAddress = "ncsoftwarecompany@gmail.com";
                service.Permissions.Create(userPermission, folderid).Execute();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }*/


            return folderid;
        }

        public async Task uploadFolder(string folderPath)
        {
            UserCredential credential;
            credential = GetCredentials();

            // Create Drive API service.
            service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            CancellationTokenSource cts = new CancellationTokenSource();
            string parentId = getParentFolderId(this.CURRENT_APP_NAME);
            string folderid = null;
            string pageToken = null;
            do
            {
                folderid = checkFolderExist(service, parentId, folderPath, ref pageToken);

            } while (pageToken != null);

            if (folderid == null)
            {
                folderid = createDriveFolder(parentId, folderPath, folderPath, service);
            }

            string[] files = Directory.GetFiles(folderPath);
            // Create multi thread
            int i = 0;
            do
            {
                List<Task> allUploadTasks = new List<Task>();
                for (int j = 0; j < 15; j++)
                {
                    if (i < files.Length)
                    {
                        allUploadTasks.Add(UploadFileToFolder(files[i], service, folderid, cts));
                    }
                    i += 1;
                }
                await Task.WhenAll(allUploadTasks.ToArray());
            } while (i < files.Length);
            // After all upload done
        }

        private string checkFolderExist(DriveService service, string parentId, string folderPath, ref string pageToken)
        {
            try
            {
                // Define parameters of request.
                FilesResource.ListRequest listRequest = service.Files.List();
                listRequest.PageSize = 100;
                //listRequest.Fields = "nextPageToken, files(id, name)";
                listRequest.Fields = "nextPageToken, files(id,name)";
                listRequest.PageToken = pageToken;
                listRequest.Q = "mimeType='application/vnd.google-apps.folder' and parents='" + parentId + "'";
                // List files.
                var request = listRequest.Execute();

                if (request.Files != null && request.Files.Count > 0)
                {
                    foreach (var file in request.Files)
                    {
                        if (file.Name == folderPath)
                        {
                            return file.Id;
                        }
                    }

                    pageToken = request.NextPageToken;

                    if (request.NextPageToken != null)
                    {
                        Console.ReadLine();
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Opps! An exception is occured " + ex.Message + "\n.Please try to close app and re-try", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private string checkFileExist(DriveService service, string parentId, string fileName, ref string pageToken)
        {
            try
            {
                // Define parameters of request.
                FilesResource.ListRequest listRequest = service.Files.List();
                listRequest.PageSize = 100;
                //listRequest.Fields = "nextPageToken, files(id, name)";
                listRequest.Fields = "nextPageToken, files(id,name)";
                listRequest.PageToken = pageToken;
                //listRequest.Q = "mimeType='"+ mineType + "' and parents='" + parentId + "'";
                listRequest.Q = "parents='" + parentId + "'";
                // List files.
                var request = listRequest.Execute();

                if (request.Files != null && request.Files.Count > 0)
                {
                    foreach (var file in request.Files)
                    {
                        if (file.Name == Path.GetFileName(fileName))
                        {
                            return file.Id;
                        }
                    }

                    pageToken = request.NextPageToken;

                    if (request.NextPageToken != null)
                    {
                        Console.ReadLine();
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Opps! An exception is occured " + ex.Message + "\n.Please try to close app and re-try", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public void DownloadFolderById(string folderId, string saveTo, string pageToken, string findText, string replaceBy)
        {

            UserCredential credential;
            credential = GetCredentials();

            // Create Drive API service.
            service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            try
            {
                // Define parameters of request.
                FilesResource.ListRequest listRequest = service.Files.List();
                listRequest.PageSize = 100;
                //listRequest.Fields = "nextPageToken, files(id, name)";
                listRequest.Fields = "nextPageToken, files(id,name)";
                listRequest.PageToken = pageToken;
                //listRequest.Q = "mimeType='"+ mineType + "' and parents='" + parentId + "'";
                listRequest.Q = "parents='" + folderId + "'";
                // List files.
                var request = listRequest.Execute();

                if (request.Files != null && request.Files.Count > 0)
                {
                    foreach (var file in request.Files)
                    {
                        DownloadAFile(service, file, saveTo+"\\"+ file.Name);
                    }

                    try
                    {
                        String fileContent = System.IO.File.ReadAllText(saveTo + "\\teescapeSetup.txt");
                        NCUtils.DataClass.TeescapeConfig teescapeObj = JsonConvert.DeserializeObject<NCUtils.DataClass.TeescapeConfig>(fileContent);
                        if(teescapeObj != null)
                        {
                            try
                            {
                                if (teescapeObj != null && teescapeObj.TeescapeItemsConfig != null && teescapeObj.TeescapeItemsConfig.Count > 0)
                                {
                                    string convertedPath = saveTo +DateTime.Now.Ticks.ToString();
                                    Directory.CreateDirectory(convertedPath);
                                    for (int i = 0; i < teescapeObj.TeescapeItemsConfig.Count; i++)
                                    {
                                        string foundImg = findImage(teescapeObj.TeescapeItemsConfig[i].ImagePath, saveTo);
                                        if (foundImg != null)
                                        {
                                            string newPath = reNameInDisk(foundImg, teescapeObj.TeescapeItemsConfig[i].Name, teescapeObj.TeescapeItemsConfig[i].TmpFullPath, 0, convertedPath, false, findText, replaceBy, false);
                                            if(newPath != null)
                                            {
                                               teescapeObj.TeescapeItemsConfig[i].ImagePath =  Path.GetFileNameWithoutExtension(newPath);
                                            }
                                        }
                                    }
                                    string stringData = JsonConvert.SerializeObject(teescapeObj);
                                    System.IO.File.WriteAllText((convertedPath+"\\teescapeSetup.txt"), stringData);
                                    System.IO.File.Delete(saveTo + "\\teescapeSetup.txt");
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Cannot rename file in disk (" + ex.Message + ")", "Error", MessageBoxButtons.OK);
                            }
                        }
                    }
                    catch (Exception) { }
                    MessageBox.Show("done");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Opps! An exception is occured " + ex.Message + "\n.Please try to close app and re-try", null, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public string findImage(string partialName, string folder)
        {
            DirectoryInfo hdDirectoryInWhichToSearch = new DirectoryInfo(@folder);
            FileInfo[] filesInDir = hdDirectoryInWhichToSearch.GetFiles(partialName + "*.*");
            if (filesInDir.Length == 1)
            {
                return filesInDir[0].FullName;
            }
            return null;
        }

        public string reNameInDisk(string foundImg, string expectName, string fullPathInJson, int fileIncrementNumber
            , string folder, Boolean copyMode
            ,string findText, string replaceBy, Boolean overwrite)
        {
            try
            {
                string rgPattern = @"[\\\/:\*\?""<>|]";
                Regex objRegEx = new Regex(rgPattern);
                string newFileName = objRegEx.Replace(expectName, "") + (fileIncrementNumber == 0 ? "" : "_" + fileIncrementNumber);
                newFileName = Regex.Replace(newFileName, findText, replaceBy, RegexOptions.IgnoreCase);
                // string newFileName = objRegEx.Replace(expectName, "");
                string newFile = folder +"\\"+ newFileName + Path.GetExtension(fullPathInJson);
                if (System.IO.File.Exists(newFile) && !overwrite)
                {
                    int nextIncrement = fileIncrementNumber + 1;
                    reNameInDisk(foundImg, expectName, fullPathInJson, nextIncrement, folder, copyMode, findText, replaceBy, overwrite);
                }
                if (copyMode)
                {
                   System.IO.File.Copy(foundImg, newFile, true);
                }
                else
                {
                    System.IO.File.Move(foundImg, newFile);
                }
                return newFile;
            }
            catch(Exception e)
            {
                Console.WriteLine("Coul dnot rename file because."+e.Message);
            }
            return null;
        }

        private static void DownloadAFile(Google.Apis.Drive.v3.DriveService service, Google.Apis.Drive.v3.Data.File file, string saveTo)
        {

            var request = service.Files.Get(file.Id);
            var stream = new System.IO.MemoryStream();

            // Add a handler which will be notified on progress changes.
            // It will notify on each chunk download and when the
            // download is completed or failed.
            request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
            {
                switch (progress.Status)
                {
                    case Google.Apis.Download.DownloadStatus.Downloading:
                        {
                            Console.WriteLine(progress.BytesDownloaded);
                            break;
                        }
                    case Google.Apis.Download.DownloadStatus.Completed:
                        {
                            Console.WriteLine("Download complete.");
                            SaveStream(stream, saveTo);
                            break;
                        }
                    case Google.Apis.Download.DownloadStatus.Failed:
                        {
                            Console.WriteLine("Download failed.");
                            break;
                        }
                }
            };
            request.Download(stream);

        }

        private static void SaveStream(System.IO.MemoryStream stream, string saveTo)
        {
            using (System.IO.FileStream file = new System.IO.FileStream(saveTo, System.IO.FileMode.Create, System.IO.FileAccess.Write))
            {
                stream.WriteTo(file);
            }
        }

        public async Task UploadSingleFile(string filePath, bool deleteAfterDone = false)
        {
            try
            {
                string folderName = Path.GetDirectoryName(filePath);
                string fileName = Path.GetFileName(filePath);

                UserCredential credential;
                credential = GetCredentials();

                CancellationTokenSource cts = new CancellationTokenSource();
                // Create Drive API service.
                DriveService service = new DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = ApplicationName,
                });
                string parentId = getParentFolderId(this.CURRENT_APP_NAME);
                string folderid = null;
                string pageToken = null;
                if (folderName.Contains("assets") || folderName.Contains("setting") || fileName.Contains(this.CURRENT_APP_NAME + "setting"))
                {
                    folderid = ASSET_FOLDERS[this.CURRENT_APP_NAME];
                }
                if (folderid == null)
                {
                    do
                    {
                        folderid = checkFolderExist(service, parentId, folderName, ref pageToken);

                    } while (pageToken != null);

                    if (folderid == null)
                    {
                        folderid = createDriveFolder(parentId, folderName, folderName, service);
                    }
                }

                //Console.WriteLine("Upload a item " + path);
                await Task.Run(() =>
                {
                    var fileMetadata = new Google.Apis.Drive.v3.Data.File();
                    fileMetadata.Name = Path.GetFileName(filePath);
                    fileMetadata.MimeType = Path.GetExtension(filePath).ToLower() == ".txt" || Path.GetExtension(filePath).ToLower() == ".json" ? "text/plain" : "image/*";

                    fileMetadata.Parents = new List<string>
                {
                    folderid
                };

                    string fileId = null;
                    pageToken = null;
                    do
                    {
                        fileId = checkFileExist(service, folderid, filePath, ref pageToken);
                    } while (pageToken != null);

                    if (fileId != null)
                    {
                        DeleteFile(service, fileId);
                    }
                    FilesResource.CreateMediaUpload request;
                    using (var stream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        request = service.Files.Create(fileMetadata, stream, fileMetadata.MimeType);
                        request.Fields = "id";
                        request.Upload();
                    }
                    var file = request.ResponseBody;
                    if (deleteAfterDone)
                    {
                        System.IO.File.Delete(filePath);
                    }

                }, cts.Token);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Upload single file has exception:", ex.Message);
            }
        }

        public async Task UploadFileToFolder(string filePath, DriveService service, string folderId, CancellationTokenSource cts)
        {
            //Console.WriteLine("Upload a item " + path);
            await Task.Run(() =>
            {
                var fileMetadata = new Google.Apis.Drive.v3.Data.File();
                fileMetadata.Name = Path.GetFileName(filePath);
                fileMetadata.MimeType = Path.GetExtension(filePath).ToLower() == ".txt" || Path.GetExtension(filePath).ToLower() == ".json" ? "text/plain" : "image/*";

                fileMetadata.Parents = new List<string>
                 {
                    folderId
                 };

                FilesResource.CreateMediaUpload request;
                using (var stream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    request = service.Files.Create(fileMetadata, stream, fileMetadata.MimeType);
                    request.Fields = "id";
                    request.Upload();
                }
                var file = request.ResponseBody;
                //Console.WriteLine("Upload item is finished "+ file.Id);
                //UpdateProccessDetail("Uploaded " + row.Name);
            }, cts.Token);
        }

        public async Task handleFolderChange(string folderPath)
        {
            try
            {
                foreach (string f in Directory.GetFiles(folderPath))
                {
                    string fileExtension = Path.GetExtension(f).ToUpper();
                    if (fileExtension == ".TXT")
                    {
                        UploadSingleFile(f);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("vncud*** handleFolderChange has exception." + ex.Message);
            }
        }

        public static void DeleteFile(DriveService service, String fileId)
        {
            try
            {
                service.Files.Delete(fileId).Execute();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred GG DeleteFile: " + e.Message);
            }
        }

        public async Task UploadFileToFolder(string filePath, string folderId, MockupRequestData mockupRequestData)
        {
            UserCredential credential;
            credential = GetCredentials();

            // Create Drive API service.
            service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            CancellationTokenSource cts = new CancellationTokenSource();
            Google.Apis.Drive.v3.Data.File file = null;
            await Task.Run(() =>
            {
                var fileMetadata = new Google.Apis.Drive.v3.Data.File();
                fileMetadata.Name = Path.GetFileName(filePath);
                fileMetadata.MimeType = Path.GetExtension(filePath).ToLower() == ".txt" || Path.GetExtension(filePath).ToLower() == ".json" ? "text/plain" : "image/*";
                fileMetadata.Parents = new List<string>
                 {
                    folderId
                 };

                FilesResource.CreateMediaUpload request;
                using (var stream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    //Waiting uploaded to google drive and get shareable link
                    void FinishedUpload(Google.Apis.Drive.v3.Data.File processedFile)
                    {
                        if (processedFile != null)
                        {
                            var shareableLink = processedFile.WebContentLink;

                            Permission userPermission = new Permission();
                            userPermission.Type = "anyone";
                            userPermission.Role = "reader";
                            service.Permissions.Create(userPermission, processedFile.Id).Execute();
                            if (shareableLink.IndexOf("&export=download") > 0)
                            {
                                shareableLink = shareableLink.Substring(0, shareableLink.IndexOf("&export=download"));
                            }
                            if (!string.IsNullOrEmpty(shareableLink))
                            {
                                // Insert into Firebase Order table
                                InsertIntoFirebaseOrderTable(shareableLink, mockupRequestData);
                            }
                        }
                    }

                    request = service.Files.Create(fileMetadata, stream, fileMetadata.MimeType);
                    request.Fields = "id,webViewLink,webContentLink";
                    request.ResponseReceived += FinishedUpload;
                    request.Upload();
                }
                // Delete converted file from PROCESSED directory
                try
                {
                    System.IO.File.Delete(filePath);
                }
                catch (Exception eer)
                {
                    Console.WriteLine("Cannot delete PROCESSED file.Ex:" + eer);
                }
                return request.ResponseBody;
            }, cts.Token);

        }

        private void InsertIntoFirebaseOrderTable(string googleDriveShareableLink, MockupRequestData mockupRequestData)
        {
            FirebaseSetup firebaseSetup = UtilsServices.ReadFile<FirebaseSetup>(UtilsServices.FIREBASE_SETUP);
            FireBaseUtils fireBaseUtils = new FireBaseUtils(firebaseSetup.FirebaseUrl, firebaseSetup.Nodes);
            Design design = new Design(googleDriveShareableLink, mockupRequestData.FileName);
            var dataJson = JsonConvert.SerializeObject(design);
            var firebaseKeyName = FireBaseUtils.buildKeyName(mockupRequestData.FileName);
            fireBaseUtils.InsertFirebaseRow(firebaseKeyName, dataJson);
        }

        private void FinishedUpload(Google.Apis.Drive.v3.Data.File file, MockupRequestData mockupRequestData)
        {
            if (file != null)
            {
                var shareableLink = file.WebContentLink;

                Permission userPermission = new Permission();
                userPermission.Type = "anyone";
                userPermission.Role = "reader";
                service.Permissions.Create(userPermission, file.Id).Execute();
                if (shareableLink.IndexOf("&export=download") > 0)
                {
                    shareableLink = shareableLink.Substring(0, shareableLink.IndexOf("&export=download"));
                }
            }
        }
    }
}
