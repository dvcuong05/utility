﻿using NCUtils.DataClass;
using NCUtils.Models;
using System;
using System.Drawing;
using System.IO;
using System.Net;

namespace NCUtils.Services
{
    public class ExportMockupByWS
    {
        public static void exportImageItemAsync(MockupRequestData mockupRequestData)
        {
            string rawImagePath = "";
            try
            {
                string imageName = MakeValidFileName(mockupRequestData.FileName);

                string exportFolder = KnownFolders.GetPath(KnownFolder.Downloads);
                string NCWebServerFolder = exportFolder + "\\NCWebServer";
                if (!Directory.Exists(NCWebServerFolder))
                {
                    Directory.CreateDirectory(NCWebServerFolder);
                }
                string downloadMockupFolder = NCWebServerFolder + "\\raw";
                if (!Directory.Exists(downloadMockupFolder))
                {
                    Directory.CreateDirectory(downloadMockupFolder);
                }
                string processedFolder = NCWebServerFolder + "\\processed";
                if (!Directory.Exists(processedFolder))
                {
                    Directory.CreateDirectory(processedFolder);
                }

                rawImagePath = @downloadMockupFolder + "\\" + imageName + ".png";

                //Download hinh ve tu URL truoc xong rezise no
                using (WebClient client = new WebClient())
                {
                    client.DownloadFile(new System.Uri(mockupRequestData.ImageUri), rawImagePath);
                }

                string processedFilePath = processedFolder + "\\" + imageName + ".png";

                int new_wid = mockupRequestData.Width != 0 ? mockupRequestData.Width : 3600;
                int new_hgt = mockupRequestData.Height != 0 ? mockupRequestData.Height : 4800;

                float dpix = mockupRequestData.Dpix != 0 ? mockupRequestData.Dpix : 300;
                float dpiy = mockupRequestData.Dpiy != 0 ? mockupRequestData.Dpiy : 300;

                FileStream stickerStream = new FileStream(rawImagePath, FileMode.Open, FileAccess.Read);
                Bitmap OriginalBitmap = (Bitmap)Image.FromStream(stickerStream);
                OriginalBitmap.SetResolution(dpix, dpiy);

                float resize_w = new_wid;
                float resize_h = new_hgt;
                float rate = (float)OriginalBitmap.Height / OriginalBitmap.Width;
                float moreX = 0;
                if (rate < ((float)12 / 9))
                {

                    resize_h = new_wid / (float)OriginalBitmap.Width * OriginalBitmap.Height;
                }
                else
                {
                    resize_w = (resize_h / (float)OriginalBitmap.Height) * OriginalBitmap.Width;
                    float alpha = new_wid - resize_w;
                    if (alpha > 0)
                    {
                        moreX = alpha / 2;
                    }
                }

                Bitmap resized = ImageUtil.Resize(OriginalBitmap, (int)resize_w, (int)resize_h);

                var k = new Kaliko.ImageLibrary.KalikoImage(resized);
                k.ApplyFilter(new Kaliko.ImageLibrary.Filters.UnsharpMaskFilter(1.4f, 0.32f, 0));

                Bitmap result = new Bitmap(new_wid, new_hgt, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                result.SetResolution(dpix, dpiy);

                using (Graphics gr = Graphics.FromImage(result))
                {
                    gr.DrawImage(k.GetAsBitmap(), (0 + moreX), 0, resize_w, resize_h);
                    result.Save(processedFilePath, System.Drawing.Imaging.ImageFormat.Png);
                    result.Dispose();
                    resized.Dispose();
                    k.Dispose();
                }
                stickerStream.Close();
                stickerStream.Dispose();

                //UPload len Order folder tren Google drive
                GoogleDrive dd = new GoogleDrive("teescape");
                dd.UploadFileToFolder(processedFilePath, mockupRequestData.OutputFolderGoogleDriveCode, mockupRequestData);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Không xuất ảnh Ành:" + mockupRequestData + ".Exception:" + ex.Message, "Lỗi");
            }
            finally
            {
                // Xoa file goc di
                try
                {
                    File.Delete(rawImagePath);
                }
                catch (Exception eer)
                {
                    Console.WriteLine("Cannot delete raw file.Ex:" + eer);
                }
            }
        }

        private static string MakeValidFileName(string name)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_");
        }
    }
}
