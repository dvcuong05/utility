﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace NCUtils.Services
{
    public class UtilsServices
    {
        public static string BASE_FOLDER = AppDomain.CurrentDomain.BaseDirectory;
        public static string FIREBASE_SETUP = BASE_FOLDER + @"\assets\firebase-setup.txt";

        public static T ReadFile<T>(string path)
        {
            try
            {
                String fileContent = File.ReadAllText(path);
                return JsonConvert.DeserializeObject<T>(fileContent);
            }
            catch
            {
                return default(T);
            }
        }
    }
}