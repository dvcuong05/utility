﻿using NCUtils;
using NCUtils.DataClass;
using NCUtils.DataClass.firebase;
using NCUtils.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.SelfHost;
using System.Windows.Forms;

namespace ResizeToll
{
    public partial class Main : Form
    {
        int currentOutput = 0;
        string TEESCAPE_EXPORT = NCUtils.Services.KnownFolders.GetPath(NCUtils.Services.KnownFolder.Downloads) + "\\export_teescape\\";
        List<string> TEESCAPE_IMG_PATHS = new List<string>();
        int convertedImaged = 0;
        public static string BASE_FOLDER = AppDomain.CurrentDomain.BaseDirectory;
        public static string LAST_GOOGLEFEED_CONFIG_PATH = BASE_FOLDER + @"\assets\lastgooglefeedseeting.json";
        string LAST_CHANGE_FILE = BASE_FOLDER + "\\lastchange.txt";
        string PROFILE_FOLDER = BASE_FOLDER + @"\assets\profiles";
        List<string> PROFILE_NAMES = new List<string>();
        FireBaseUtils fireBaseUtils;
        string _baseAddress = "http://localhost:2095/";
        // Create server
        HttpSelfHostServer selfServer = null;


        public Main()
        {
            InitializeComponent();
            
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            //Process.Start("chrome.exe", "https://www.amazon.com/dp/B07D7W9SY9");
            currentOutput = 0;
            if (this.checkBoxClearFolder.Checked)
            {
                if (Directory.Exists(this.textBoxOutputFolder.Text))
                {
                    Directory.Delete(this.textBoxOutputFolder.Text, true);
                }
            }
            if (!Directory.Exists(this.textBoxOutputFolder.Text))
            {
                Directory.CreateDirectory(this.textBoxOutputFolder.Text);
            }

            string[] files = System.IO.Directory.GetFiles(this.textBoxInputFolder.Text);
            foreach(string path in files)
            {
                Resize(path);
            }
            MessageBox.Show("Done !!");
        }

        public void Resize(string filePath)
        {
            try
            {
                FileStream myFileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                Image image = Image.FromStream(myFileStream, true, false);

                var newWidth = int.Parse(this.textBoxW.Text);
                var newHeight = int.Parse(this.textBoxH.Text);
                var thumbnailBitmap = new Bitmap(newWidth, newHeight);

                var thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
                thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbnailGraph.DrawImage(image, imageRectangle);

                string starget = this.textBoxOutputFolder.Text + "\\" + Path.GetFileName(filePath);
                FileStream toStream = new FileStream(@starget, FileMode.Create, FileAccess.ReadWrite);
                thumbnailBitmap.Save(toStream, image.RawFormat);

                thumbnailGraph.Dispose();
                thumbnailBitmap.Dispose();
                image.Dispose();
                currentOutput = currentOutput + 1;
                this.labelTotalOut.Text = currentOutput + "";
            }
            catch(Exception ex)
            {
                Console.WriteLine("Resize has exception " + ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.textBoxW.Text = "4500";
            this.textBoxH.Text = "5400";
            this.textBoxInputFolder.Text = @"C:\Users\Administrator\Downloads\input";
            this.textBoxOutputFolder.Text = @"C:\Users\Administrator\Downloads\output";

            try
            {
                string[] values = Directory.GetFiles(PROFILE_FOLDER);
                foreach(string item in values)
                {
                    PROFILE_NAMES.Add(Path.GetFileName(item));
                }
                this.comboBoxProfile.DataSource = PROFILE_NAMES;
            }
            catch(Exception ee) { }

            FirebaseSetup firebaseSetup = this.ReadFile<FirebaseSetup>(NCUtils.Services.UtilsServices.FIREBASE_SETUP);
            fireBaseUtils = new FireBaseUtils(firebaseSetup.FirebaseUrl, firebaseSetup.Nodes);

            loadConfig();
            startSelfWebservice();
        }

        private void startSelfWebservice()
        {
            //var config = new EExtendHttpSelfHostConfigurationpublic(_baseAddress);

            //    config.Routes.MapHttpRoute(
            //        name: "DefaultApi",
            //        routeTemplate: "api/{controller}/{id}",
            //        defaults: new { id = RouteParameter.Optional }
            //    );

            //    using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            //    {
            //        server.OpenAsync().Wait();

            //    }
            //// Set up server configuration
            HttpSelfHostConfiguration config = new HttpSelfHostConfiguration(_baseAddress);

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Create server
            selfServer = new HttpSelfHostServer(config);

            // Start listening
            selfServer.OpenAsync().Wait();
        }

        private void loadConfig()
        {
            try
            {
                string filePath = Path.Combine(PROFILE_FOLDER, this.comboBoxProfile.Text.EndsWith(".txt") ? this.comboBoxProfile.Text : this.comboBoxProfile.Text + ".txt");
                LastChanges config = this.ReadFile<LastChanges>(filePath);
                textBoxParentFolderDrive.Text = config.TextBoxParentFolderDrive;
                textBoxOutputDriveRename.Text = config.TextBoxOutputDriveRename;
                textBoxTrackingFilePath.Text = config.TextBoxTrackingFilePath;
                textBoxSheetId.Text = config.TextBoxSheetId;
                textBoxDBIP.Text = config.TextBoxDBIP;
                textBoxDBUser.Text = config.TextBoxDBUser;
                textBoxDBPass.Text = config.TextBoxDBPass;
                textBoxDBName.Text = config.TextBoxDBName;
                textBoxSiteAddress.Text = config.TextBoxSiteAddress;
                textBoxFolderTeescape.Text = config.TextBoxFolderTeescape;
                checkBoxIsBingAds.Checked = config.IsBingAds;
            }
            catch (Exception ex) { }
        }

        public T ReadFile<T>(string path)
        {
            try
            {
                String fileContent = File.ReadAllText(path);
                return JsonConvert.DeserializeObject<T>(fileContent);
            }
            catch
            {
                return default(T);
            }
        }

        private void textBoxInputFolder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string[] files = System.IO.Directory.GetFiles(this.textBoxInputFolder.Text);
                this.labelTotalInput.Text = files.Length + "";
            }
            catch (Exception) { }
        }

        private void checkBoxHoodie_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBoxHoodie.Checked)
            {
                this.textBoxW.Text = "4500";
                this.textBoxH.Text = "4050";
            }
        }

        private void buttonStartClean_Click(object sender, EventArgs e)
        {
            string currentFolder="";
            string[] folders = Directory.GetDirectories(textBoxInputCleanDirectory.Text);
            foreach (string folder in folders)
            {
                try
                {
                    if (Directory.GetFiles(@folder).Length <= int.Parse(this.textBoxMinFile.Text))
                    {
                        currentFolder = @folder;
                        DeleteDirectory(@folder, true);
                        this.labelTotalDeleted.Text = (int.Parse(this.labelTotalDeleted.Text) + 1).ToString();
                    }
                }
                catch (Exception ee)
                {
                    //MessageBox.Show("Has error." + ee.Message);
                    string folderName = Path.GetFileName(currentFolder);
                    Directory.Move(currentFolder, textBoxInputCleanDirectory.Text + "//checking//" + folderName);
                }
            }
        }

        public void DeleteDirectory(string path, bool recursive)
        {
            if (recursive)
            {
                var subfolders = Directory.GetDirectories(path);
                foreach (var s in subfolders)
                {
                    DeleteDirectory(s, recursive);
                }
            }
            var files = Directory.GetFiles(path);
            foreach (var f in files)
            {
                try
                {
                    var attr = File.GetAttributes(f);
                    if ((attr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        File.SetAttributes(f, attr ^ FileAttributes.ReadOnly);
                    }
                    File.Delete(f);
                }
                catch (IOException ee)
                {
                    MessageBox.Show("Has error DeleteDirectory " + ee.Message);
                }
            }

            // At this point, all the files and sub-folders have been deleted.
            // So we delete the empty folder using the OOTB Directory.Delete method.
            Directory.Delete(path, true);
        }

        private void buttonDriveDownload_Click(object sender, EventArgs e)
        {
            try
            {
                GoogleDrive gg = new GoogleDrive("");
                
                string pageToken = null;
                gg.DownloadFolderById(this.textBoxDriveGuid.Text, this.textBoxDownloadTo.Text, pageToken, textBoxFindText.Text, textBoxReplaceBy.Text);
                //gg.downloadFolder("1htVYa5P11OquoKaqlrwwxh2b0iAHOi-k");

            }
            catch(Exception ee)
            {
                MessageBox.Show("Download fail." + ee.Message);
            }
        }

        private void buttonStartConvert_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime d2 = DateTime.Parse(this.textBoxDateInput.Text, null, System.Globalization.DateTimeStyles.RoundtripKind);
                this.textBoxConvertResult.Text = d2.Ticks.ToString();
            }
            catch (Exception ee)
            {
                MessageBox.Show("Convert fail." + ee.Message);
            }
        }

        private void textBoxFolderTeescape_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string[] fileEntries = Directory.GetFiles(textBoxFolderTeescape.Text);
                int countTotal = 0;
                TEESCAPE_IMG_PATHS.Clear();
                foreach (string filePath in fileEntries)
                {
                    String extension = Path.GetExtension(filePath).ToLower();
                    if (extension == ".jpg" || extension == ".jpeg" || extension == ".png" || extension == ".eps" || extension == "gif")
                    {
                        countTotal = countTotal + 1;
                        TEESCAPE_IMG_PATHS.Add(filePath);
                    }
                }
                labelTotalFile.Text = countTotal.ToString();
            }
            catch (Exception) { }
        }

        private void reNameInDisk(string foundImg, string expectName, string fullPathInJson, int fileIncrementNumber)
        {
            string rgPattern = @"[\\\/:\*\?""<>|]";
            Regex objRegEx = new Regex(rgPattern);
            string newFileName = objRegEx.Replace(expectName, "");
            string newFile = @textBoxFolderTeescape.Text + "\\" + newFileName + Path.GetExtension(fullPathInJson);
            if (File.Exists(newFile))
            {
                int nextIncrement = fileIncrementNumber + 1;
                reNameInDisk(foundImg, expectName, fullPathInJson, nextIncrement);
            }
            System.IO.File.Move(foundImg, newFile);
        }

        bool isExporting = false;
        private void buttonExport_Click(object sender, EventArgs e)
        {
            if (isExporting)
            {
                return;
            }
            isExporting = true;
            string folderName = DateTime.Now.Ticks.ToString();
            string exportFolder = textBoxFolderTeescape.Text.EndsWith("\\") ? (textBoxFolderTeescape.Text + folderName) : (textBoxFolderTeescape.Text + "\\" + folderName);
            if (!Directory.Exists(exportFolder))
            {
                Directory.CreateDirectory(exportFolder);
            }

            //Loop and export list image in current folder
            foreach(string path in TEESCAPE_IMG_PATHS)
            {
                exportImageItem(exportFolder,path);
            }
            isExporting = false;
            MessageBox.Show("Xong! Ngon lành");
        }

        private void exportImageItem(string exportFolder, string imgPath)
        {
            try
            {
                string imageName = Path.GetFileName(imgPath);
                labelCurrentImageName.Text = imageName;
                string downloadsPath = exportFolder + "\\" + imageName;

                int new_wid = int.Parse(textBoxWidth.Text);
                int new_hgt = int.Parse(textBoxHeight.Text);

                float dpix = float.Parse(textBoxDpiX.Text);
                float dpiy = float.Parse(textBoxDpiY.Text);

                FileStream stickerStream = new System.IO.FileStream(imgPath, FileMode.Open, FileAccess.Read);
                Bitmap OriginalBitmap= (Bitmap)Image.FromStream(stickerStream);

               /* using (Graphics gr = Graphics.FromImage(OriginalBitmap))
                {
                    labelDpiX.Text = gr.DpiX.ToString();
                    labelDpiY.Text = gr.DpiY.ToString();

                }
                labelW.Text = OriginalBitmap.Width.ToString();
                labelH.Text = OriginalBitmap.Height.ToString();    */            
                OriginalBitmap.SetResolution(dpix, dpiy);

                float resize_w = new_wid;
                float resize_h = new_hgt;
                float rate = (float)OriginalBitmap.Height / OriginalBitmap.Width;
                float moreX = 0;
                if (rate < ((float)12 / 9))
                {

                    resize_h = new_wid / (float)OriginalBitmap.Width * OriginalBitmap.Height;
                }
                else
                {
                    resize_w = (resize_h / (float)OriginalBitmap.Height) * OriginalBitmap.Width;
                    float alpha = new_wid - resize_w;
                    if (alpha > 0)
                    {
                        moreX = alpha / 2;
                    }
                }

                Bitmap resized = ImageUtil.Resize(OriginalBitmap, (int)resize_w, (int)resize_h);
                
                var k = new Kaliko.ImageLibrary.KalikoImage(resized);
                k.ApplyFilter(new Kaliko.ImageLibrary.Filters.UnsharpMaskFilter(1.4f, 0.32f, 0));

                Bitmap result = new Bitmap(new_wid, new_hgt, PixelFormat.Format32bppArgb);
                result.SetResolution(dpix, dpiy);

                using (Graphics gr = Graphics.FromImage(result))
                {
                    gr.DrawImage(k.GetAsBitmap(), (0 + moreX), 0, resize_w, resize_h);


                    result.Save(downloadsPath, ImageFormat.Png);
                    System.Media.SystemSounds.Beep.Play();
                    result.Dispose();
                    resized.Dispose();
                    k.Dispose();
                    // sharpedImg.Dispose();
                }
                convertedImaged = convertedImaged + 1;
                //labelProcessFile.Text = convertedImaged.ToString();
                stickerStream.Close();
                stickerStream.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Không xuất ảnh Ành:"+ imgPath+".Exception:" + ex.Message, "Lỗi");
            }
        }


        private string findImage(string partialName)
        {
            DirectoryInfo hdDirectoryInWhichToSearch = new DirectoryInfo(@textBoxFolderTeescape.Text);
            FileInfo[] filesInDir = hdDirectoryInWhichToSearch.GetFiles(partialName + "*.*");
            if (filesInDir.Length == 1)
            {
                return filesInDir[0].FullName;
            }
            return null;
        }

        private void buttonFromClipboard_Click(object sender, EventArgs e)
        {
            textBoxFolderTeescape.Text = Clipboard.GetText();
        }

        private void buttonDownloadAndExport_Click(object sender, EventArgs e)
        {
            string namesInString = richTextBoxImageNames.Text;
            if(textBoxFolderTeescape.Text == null || textBoxFolderTeescape.Text == "")
            {
                MessageBox.Show("Vui lòng chọn folder để lưu hình trước!");
                return;
            }
            if (namesInString == null || namesInString == "")
            {
                MessageBox.Show("Vui lòng nhập danh sách tên hình cần download phân cách bởi dấu ','");
                return;
            }
            string[] imageNames = namesInString.Split(',');
            foreach(string item in imageNames)
            {
                AmazonS3 s3 = new AmazonS3();
                s3.downloadFile(item, textBoxFolderTeescape.Text);
            }

            textBoxFolderTeescape_TextChanged(null, null);
            buttonExport_Click(null, null);
        }

        DateTime EndOfTime;
        int totalMin;
        private void buttonStartFeed_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            totalMin = Int32.Parse(textBoxInterval.Text);
            timer1.Interval = totalMin * 60 * 1000;

            EndOfTime = DateTime.Now.AddMinutes(totalMin);
            Timer t = new Timer() { Interval = 500, Enabled = true };
            t.Tick += new EventHandler(timer2_Tick);
            timer2_Tick(null, null);

            string sheetID = textBoxSheetId.Text;
            string siteName = textBoxSiteAddress.Text;
            string host = textBoxDBIP.Text;
            string user = textBoxDBUser.Text;
            string pass = textBoxDBPass.Text;
            string dbName = textBoxDBName.Text;
            string rage = textBoxRange.Text;

            LastGoogleFeedSetting lastGoogleFeedSetting = new LastGoogleFeedSetting();
            lastGoogleFeedSetting.SheetID = textBoxSheetId.Text;
            lastGoogleFeedSetting.SiteName = textBoxSiteAddress.Text;
            lastGoogleFeedSetting.Host = textBoxDBIP.Text;
            lastGoogleFeedSetting.User = textBoxDBUser.Text;
            lastGoogleFeedSetting.Pass = textBoxDBPass.Text;
            lastGoogleFeedSetting.DbName = textBoxDBName.Text;
            lastGoogleFeedSetting.Rage = textBoxRange.Text;

            File.WriteAllText(LAST_GOOGLEFEED_CONFIG_PATH, JsonConvert.SerializeObject(lastGoogleFeedSetting));

            SheetsSample s = new SheetsSample(siteName,sheetID,host,dbName,user,pass, rage);
            s.StartFeeds(this.checkBoxIsBingAds.Checked);

            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            EndOfTime = DateTime.Now.AddMinutes(totalMin);

            string sheetID = textBoxSheetId.Text;
            string siteName = textBoxSiteAddress.Text;
            string host = textBoxDBIP.Text;
            string user = textBoxDBUser.Text;
            string pass = textBoxDBPass.Text;
            string dbName = textBoxDBName.Text;
            string rage = textBoxRange.Text;

            SheetsSample s = new SheetsSample(siteName, sheetID, host, dbName, user, pass, rage);
            s.StartFeeds(this.checkBoxIsBingAds.Checked);
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            TimeSpan ts = EndOfTime.Subtract(DateTime.Now);
            string s = string.Format("{0:d2}:{1:d2}", ts.Minutes, ts.Seconds);
            labelCounter.Text = s;
        }

        private void buttonRenameDrive_Click(object sender, EventArgs e)
        {
            GoogleDrive dd = new GoogleDrive("");
            List<string> trackingFile = new List<string>();
            List<string> uploadedFiles = new List<string>();
            List<TeescapeConfig> teescapeConfigFromFiles = new List<TeescapeConfig>();
            List<NCUtils.DataClass.merch.UploadConfig> merchConfigFromFiles = new List<NCUtils.DataClass.merch.UploadConfig>();

            if (textBoxTrackingFilePath.Text != null && textBoxTrackingFilePath.Text != "")
            {
                String fileContent = File.ReadAllText(@textBoxTrackingFilePath.Text);
                trackingFile = JsonConvert.DeserializeObject<List<string>>(fileContent);
                if (trackingFile == null)
                {
                    trackingFile = new List<string>();
                }
            }

            if (checkBoxFirebaseTracking.Checked)
            {
                string[] fileEntries = Directory.GetFiles(textBoxParentFolderDrive.Text);
                foreach (string filePath in fileEntries)
                {
                    String extension = Path.GetExtension(filePath).ToLower();
                    if (extension == ".jpg" || extension == ".jpeg" || extension == ".png" || extension == ".eps" || extension == "gif")
                    {
                        var firebaseKey = Path.GetFileNameWithoutExtension(filePath);
                        string name = fireBaseUtils.findPictureName(firebaseKey);
                        if (!string.IsNullOrEmpty(name) && !trackingFile.Contains(name.Replace("'", "&#39;").Trim()) && !trackingFile.Contains(name.Trim()))
                        {
                            if (name != null)
                            {
                                dd.reNameInDisk(filePath, name, filePath, 0, textBoxOutputDriveRename.Text, true, textBoxFindText.Text, textBoxReplaceBy.Text, true);
                                trackingFile.Add(name.Trim());
                            }
                        }else if (!string.IsNullOrEmpty(name))
                        {
                            uploadedFiles.Add(name);
                        }
                    }
                }
                //Download if file name could not a key in firebase.. khi hinh lay tu KAK tee chang han
                fireBaseUtils.downloadFileFromURL(textBoxOutputDriveRename.Text, trackingFile);
            }
            else
            {
                string[] listSubFolders = Directory.GetDirectories(textBoxParentFolderDrive.Text);
                foreach (string sub in listSubFolders)
                {
                    string currenFolder = sub;
                    try
                    {
                        if (!sub.EndsWith("\\") && sub.EndsWith("/"))
                        {
                            currenFolder = currenFolder + "\\";
                        }

                        //Lay het cac file teescapeSetup trong 1 folder co the nhiu hon mot file
                        teescapeConfigFromFiles = new List<TeescapeConfig>();
                        DirectoryInfo d = new DirectoryInfo(@currenFolder);//Assuming Test is your Folder
                        FileInfo[] listTeescapeSetupFiles = d.GetFiles("teescapeSetup*"); //Getting Text files are teescapeSetup_xxx
                        if(listTeescapeSetupFiles.Length > 0)
                        {
                            foreach(FileInfo fi in listTeescapeSetupFiles)
                            {
                                try
                                {
                                    teescapeConfigFromFiles.Add(JsonConvert.DeserializeObject<TeescapeConfig>(File.ReadAllText(@fi.FullName)));
                                }
                                catch (Exception ee) { }
                            }
                        }

                        merchConfigFromFiles = new List<NCUtils.DataClass.merch.UploadConfig>();
                        FileInfo[] listMerchSetupFiles = d.GetFiles("merchSetup*"); //Getting Text files are merchSetup_xxx
                        if (listMerchSetupFiles.Length > 0)
                        {
                            foreach (FileInfo fi in listMerchSetupFiles)
                            {
                                try
                                {
                                    merchConfigFromFiles.Add(JsonConvert.DeserializeObject<NCUtils.DataClass.merch.UploadConfig>(File.ReadAllText(fi.FullName)));
                                }
                                catch (Exception ee) { }
                            }
                        }


                        if (teescapeConfigFromFiles.Count > 0)
                        {
                            foreach (TeescapeConfig teescapeObj in teescapeConfigFromFiles)
                            {
                                try
                                {
                                    if (teescapeObj != null && teescapeObj.TeescapeItemsConfig != null && teescapeObj.TeescapeItemsConfig.Count > 0)
                                    {
                                        string convertedPath = Path.Combine((!textBoxOutputDriveRename.Text.EndsWith("\\") && !textBoxOutputDriveRename.Text.EndsWith("/")) ? textBoxOutputDriveRename.Text + "\\" : textBoxOutputDriveRename.Text, new DirectoryInfo(currenFolder).Name);// + DateTime.Now.Ticks.ToString();
                                        if (!Directory.Exists(convertedPath))
                                        {
                                            Directory.CreateDirectory(convertedPath);
                                        }
                                        for (int i = 0; i < teescapeObj.TeescapeItemsConfig.Count; i++)
                                        {
                                            string foundImg = dd.findImage(teescapeObj.TeescapeItemsConfig[i].ImagePath, sub);
                                            if (foundImg != null && !trackingFile.Contains(teescapeObj.TeescapeItemsConfig[i].Name) && !trackingFile.Contains(teescapeObj.TeescapeItemsConfig[i].Name.Replace("'", "&#39;")))
                                            {
                                                trackingFile.Add(teescapeObj.TeescapeItemsConfig[i].Name);

                                                string newPath = dd.reNameInDisk(foundImg, teescapeObj.TeescapeItemsConfig[i].Name, teescapeObj.TeescapeItemsConfig[i].TmpFullPath, 0, convertedPath, true, textBoxFindText.Text, textBoxReplaceBy.Text, false);
                                                if (newPath != null)
                                                {
                                                    teescapeObj.TeescapeItemsConfig[i].ImagePath = Path.GetFileNameWithoutExtension(newPath);
                                                }
                                            }
                                            else if (foundImg != null)
                                            {
                                                uploadedFiles.Add(teescapeObj.TeescapeItemsConfig[i].Name);
                                            }
                                        }
                                        //Put to keep tracking
                                        // trackingFile.Add(teescapeObj.FolderPath);

                                        string stringData = JsonConvert.SerializeObject(teescapeObj);
                                        System.IO.File.WriteAllText(Path.Combine(convertedPath, "teescapeSetup.txt"), stringData);
                                        // System.IO.File.Delete(saveTo + "\\teescapeSetup.txt");
                                    }
                                    else
                                    {
                                        copyFileWhenNotSetupTXT(currenFolder, sub, dd, trackingFile, uploadedFiles);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Cannot rename file in disk (" + ex.Message + ")", "Error", MessageBoxButtons.OK);
                                }
                            }
                        }

                        if (merchConfigFromFiles.Count > 0)
                        {
                            foreach (NCUtils.DataClass.merch.UploadConfig merchObj in merchConfigFromFiles)
                            {
                                try
                                {
                                    if (merchObj.ItemsConfig != null && merchObj.ItemsConfig.Count > 0)
                                    {
                                        string convertedPath = (!textBoxOutputDriveRename.Text.EndsWith("\\") && !textBoxOutputDriveRename.Text.EndsWith("/")) ? textBoxOutputDriveRename.Text + "\\" : textBoxOutputDriveRename.Text + new DirectoryInfo(currenFolder).Name;// + DateTime.Now.Ticks.ToString();
                                        Directory.CreateDirectory(convertedPath);
                                        for (int i = 0; i < merchObj.ItemsConfig.Count; i++)
                                        {
                                            string foundImg = dd.findImage(merchObj.ItemsConfig[i].ImagePath, sub);
                                            if (foundImg != null && !trackingFile.Contains(merchObj.ItemsConfig[i].Name) && !trackingFile.Contains(merchObj.ItemsConfig[i].Name.Replace("'", "&#39;")))
                                            {
                                                trackingFile.Add(merchObj.ItemsConfig[i].Name);

                                                string newPath = dd.reNameInDisk(foundImg, merchObj.ItemsConfig[i].Name, merchObj.ItemsConfig[i].TmpFullPath, 0, convertedPath, true, textBoxFindText.Text, textBoxReplaceBy.Text, false);
                                                if (newPath != null)
                                                {
                                                    merchObj.ItemsConfig[i].ImagePath = Path.GetFileNameWithoutExtension(newPath);
                                                }
                                            }
                                            else if (foundImg != null)
                                            {
                                                uploadedFiles.Add(merchObj.ItemsConfig[i].Name);
                                            }
                                        }

                                        //Put to keep tracking
                                        //trackingFile.Add(merchObj.FolderPath);

                                        string stringData = JsonConvert.SerializeObject(merchObj);
                                        System.IO.File.WriteAllText(Path.Combine(convertedPath, "merchSetup.txt"), stringData);
                                    }
                                    else
                                    {
                                        copyFileWhenNotSetupTXT(currenFolder, sub, dd, trackingFile, uploadedFiles);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Cannot rename file in disk (" + ex.Message + ")", "Error", MessageBoxButtons.OK);
                                }
                            }
                            
                        }
                        if(merchConfigFromFiles.Count == 0 && teescapeConfigFromFiles.Count == 0)
                        {
                            copyFileWhenNotSetupTXT(currenFolder, sub, dd, trackingFile, uploadedFiles);
                        }
                    }
                    catch (Exception exx) { Console.WriteLine("Could not rename" + exx.Message); }
                }

                if (this.checkBoxMergeAfterDone.Checked)
                {
                    listSubFolders = Directory.GetDirectories(textBoxOutputDriveRename.Text);
                    foreach (string sub in listSubFolders)
                    {
                        string currenFolder = sub;
                        try
                        {
                            if (sub.EndsWith("\\") || sub.EndsWith("/"))
                            {

                            }
                            else
                            {
                                currenFolder = currenFolder + "\\";
                            }
                            {
                                string convertedPath = textBoxOutputDriveRename.Text + "\\merged";
                                if (!Directory.Exists(convertedPath))
                                {
                                    Directory.CreateDirectory(convertedPath);
                                }
                                string[] fileEntries = Directory.GetFiles(sub);
                                foreach (string filePath in fileEntries)
                                {
                                    {
                                        var fileP = Path.GetFileNameWithoutExtension(filePath);
                                        {
                                            dd.reNameInDisk(filePath, fileP
                                            , filePath, 0, convertedPath, true, textBoxFindText.Text, textBoxReplaceBy.Text, true);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception exx) { Console.WriteLine("Could not rename" + exx.Message); }
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(textBoxTrackingFilePath.Text))
            {
                //Put to keep tracking
                string stringData = JsonConvert.SerializeObject(trackingFile);
                System.IO.File.WriteAllText(textBoxTrackingFilePath.Text, stringData);
            }
            if(uploadedFiles.Count > 0){
                //Put to keep tracking uploaded file
                string stringData = JsonConvert.SerializeObject(uploadedFiles);
                System.IO.File.WriteAllText(textBoxOutputDriveRename.Text + "\\ByPassUpload.txt", stringData);
            }
            MessageBox.Show("Done");
        }

        private void copyFileWhenNotSetupTXT(string currenFolder, string sub, GoogleDrive dd, List<string> trackingFiles, List<string> uploadedFiles)
        {
            string convertedPath = textBoxOutputDriveRename.Text + "\\" + new DirectoryInfo(currenFolder).Name;// + DateTime.Now.Ticks.ToString();
            Directory.CreateDirectory(convertedPath);

            string[] fileEntries = Directory.GetFiles(sub);
            foreach (string filePath in fileEntries)
            {
                if (!trackingFiles.Contains(Path.GetFileNameWithoutExtension(filePath)))
                {
                    String extension = Path.GetExtension(filePath).ToLower();
                    if (extension == ".jpg" || extension == ".jpeg" || extension == ".png" || extension == ".eps" || extension == "gif")
                    {
                        dd.reNameInDisk(filePath, Path.GetFileNameWithoutExtension(filePath)
                            , filePath, 0, convertedPath, true, textBoxFindText.Text, textBoxReplaceBy.Text, false);
                        trackingFiles.Add(Path.GetFileNameWithoutExtension(filePath));
                    }
                }
                else
                {
                    uploadedFiles.Add(Path.GetFileNameWithoutExtension(filePath));
                }
            }
        }

        private void textBoxParentFolderDrive_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (checkBoxFirebaseTracking.Checked)
                {
                    labelDownloaded.Text = Directory.GetFiles(textBoxParentFolderDrive.Text).Length.ToString();
                    return;
                }
                string[] listSubFolders = Directory.GetDirectories(@textBoxParentFolderDrive.Text);
                labelDownloaded.Text = listSubFolders.Length.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Load files has exception.Exception:"+ex.Message);
            }
        }

        private void textBoxInputCleanDirectory_TextChanged(object sender, EventArgs e)
        {
            string[] listSubFolders = Directory.GetDirectories(textBoxInputCleanDirectory .Text);
            labelDownloaded.Text = listSubFolders.Length.ToString();
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            saveProfile();
            //Clsoe Self webservice
            selfServer.CloseAsync();
        }

        private void saveProfile()
        {
            try
            {
                LastChanges config = new LastChanges();
                config.TextBoxParentFolderDrive = textBoxParentFolderDrive.Text;
                config.TextBoxParentFolderDrive = textBoxParentFolderDrive.Text;
                config.TextBoxOutputDriveRename = textBoxOutputDriveRename.Text;
                config.TextBoxTrackingFilePath = textBoxTrackingFilePath.Text;
                config.TextBoxSheetId = textBoxSheetId.Text;
                config.TextBoxDBIP = textBoxDBIP.Text;
                config.TextBoxDBUser = textBoxDBUser.Text;
                config.TextBoxDBPass = textBoxDBPass.Text;
                config.TextBoxDBName = textBoxDBName.Text;
                config.TextBoxSiteAddress = textBoxSiteAddress.Text;
                config.TextBoxFolderTeescape = textBoxFolderTeescape.Text;

                string filePath = Path.Combine(PROFILE_FOLDER,this.comboBoxProfile.Text.EndsWith(".txt") ? this.comboBoxProfile.Text : this.comboBoxProfile.Text+".txt");
                string stringData = JsonConvert.SerializeObject(config);
                System.IO.File.WriteAllText(filePath, stringData);
                MessageBox.Show("Done");
            }
            catch(Exception ee)
            {

            }
        }

        private void textBoxParentFolderDrive_DoubleClick(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", textBoxParentFolderDrive.Text);
        }

        private void textBoxOutputDriveRename_DoubleClick(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", textBoxOutputDriveRename.Text);
        }

        private void buttonSaveProfile_Click(object sender, EventArgs e)
        {
            saveProfile();
        }

        private void buttonRemoveProfile_Click(object sender, EventArgs e)
        {
            try
            {
                string filePath = Path.Combine(PROFILE_FOLDER, this.comboBoxProfile.Text);
                File.Delete(filePath);
                PROFILE_NAMES.Clear();
                string[] values = Directory.GetFiles(PROFILE_FOLDER);
                foreach (string item in values)
                {
                    PROFILE_NAMES.Add(Path.GetFileName(item));
                }
                this.comboBoxProfile.DataSource = null;
                this.comboBoxProfile.DataSource = PROFILE_NAMES;
                MessageBox.Show("Done");
            }
            catch (Exception ee)
            {

            }
        }

        private void comboBoxProfile_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadConfig();
        }

        private void textBoxFolderTeescape_DoubleClick(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", textBoxFolderTeescape.Text);
        }

        private void textBoxTrackingFilePath_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(@textBoxTrackingFilePath.Text);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
