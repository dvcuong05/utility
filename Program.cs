﻿using NCUtils.Services;
using System;
using System.Windows.Forms;

namespace ResizeToll
{
    static class Program
    {
        static void Main(string[] args)
        {
            //var assembly = typeof(Program).Assembly;
            //var attribute = (GuidAttribute)assembly.GetCustomAttributes(typeof(GuidAttribute), true)[0];
            //var id = attribute.Value;
            //Console.WriteLine(id);
            //startSelfWebservice();
            //return;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
        }

    }
}
