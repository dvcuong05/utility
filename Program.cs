﻿using NCUtils.Services;
using System;
using System.Runtime.InteropServices;
using System.Web.Http;
using System.Web.Http.SelfHost;
using System.Windows.Forms;

namespace ResizeToll
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        //[STAThread]
        
        static void Main(string[] args)
        {
            var assembly = typeof(Program).Assembly;
            var attribute = (GuidAttribute)assembly.GetCustomAttributes(typeof(GuidAttribute), true)[0];
            var id = attribute.Value;
            Console.WriteLine(id);
            //startSelfWebservice();
            //return;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
        }

        private static void startSelfWebservice()
        {
            var config = new EExtendHttpSelfHostConfigurationpublic("https://localhost:2095/");

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            {
                server.OpenAsync().Wait();
                Console.WriteLine("Press Enter to quit.");
                Console.ReadLine();

            }
            //// Set up server configuration
            //HttpSelfHostConfiguration config = new HttpSelfHostConfiguration(_baseAddress);

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            //// Create server
            //selfServer = new HttpSelfHostServer(config);

            //// Start listening
            //selfServer.OpenAsync().Wait();
        }
    }
}
