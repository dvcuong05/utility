﻿using System;
using NCUtils.DataClass.merch;

namespace Main
{
    public class ItemConfig
    {

        private string tmpFullPath;
        private String imagePath;
        private String name;
        private bool isFinished;
        private String mainkeyword;
        private string addKey;
        private String[] replaceKeywords;
        private AMZConfig amzConfig;

        
        public ItemConfig()
        {
            // TODO: Complete member initialization
        }

        public ItemConfig(String _imagePath, String _name)
        {
            this.imagePath = _imagePath;
            this.name = _name;
        }

        public String ImagePath
        {
            get { return imagePath; }
            set { imagePath = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }
       
        public bool IsFinished
        {
            get { return isFinished; }
            set { isFinished = value; }
        }
        public string TmpFullPath
        {
            get { return tmpFullPath; }
            set { tmpFullPath = value; }
        }
        public string[] ReplaceKeywords
        {
            get
            {
                return replaceKeywords;
            }

            set
            {
                replaceKeywords = value;
            }
        }
        public AMZConfig AmzConfig { get => amzConfig; set => amzConfig = value; }
        public string Mainkeyword { get => mainkeyword; set => mainkeyword = value; }
        public string AddKey { get => addKey; set => addKey = value; }
    }
}
