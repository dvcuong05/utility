﻿using System.Collections.Generic;

namespace Main.classes.merch
{
    public class ShirtConfig
    {
        string name;
        string code;
        bool isSelected = false;
        double price;
        double minPrice;
        List<FitType> fitTypes = new List<FitType>();
        List<Market> markets = new List<Market>();
        List<Color> colors = new List<Color>();

        public string Name { get => name; set => name = value; }
        public string Code { get => code; set => code = value; }
        public bool IsSelected { get => isSelected; set => isSelected = value; }
        public List<FitType> FitTypes { get => fitTypes; set => fitTypes = value; }
        public List<Market> Markets { get => markets; set => markets = value; }
        public List<Color> Colors { get => colors; set => colors = value; }
        public double Price { get => price; set => price = value; }
        public double MinPrice { get => minPrice; set => minPrice = value; }
    }

    public class FitType
    {
        string code;
        string name;
        bool isSelected = false;
        bool isDisable = false;
        public FitType() { }

        public string Code { get => code; set => code = value; }
        public string Name { get => name; set => name = value; }
        public bool IsSelected { get => isSelected; set => isSelected = value; }
        public bool IsDisable { get => isDisable; set => isDisable = value; }
    }

    public class Market
    {
        string code;
        string name;
        bool isSelected = false;
        bool isDisable = false;
        public Market() { }

        public string Code { get => code; set => code = value; }
        public string Name { get => name; set => name = value; }
        public bool IsSelected { get => isSelected; set => isSelected = value; }
        public bool IsDisable { get => isDisable; set => isDisable = value; }
    }

    public class Color
    {
        string code;
        string name;
        bool isSelected = false;
        public Color() { }

        public string Code { get => code; set => code = value; }
        public string Name { get => name; set => name = value; }
        public bool IsSelected { get => isSelected; set => isSelected = value; }
    }

}
