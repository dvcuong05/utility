﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCUtils.DataClass
{
    public class TeescapeStyle
    {
        private string id;
        private string price;
        private string displayedName;
        private string preName;
        private string description;
        private string label;
        private bool isChecked = false;
        private string identifier;
        private string configFolderPath;
        private string amzCategory;
        private int total = 0;
        private string productId;

        public bool IsChecked
        {
            get { return isChecked; }
            set { isChecked = value; }
        }

        public string Price
        {
            get { return price; }
            set { price = value; }
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Label
        {
            get { return label; }
            set { label = value; }
        }

        public int Total
        {
            get { return total; }
            set { total = value; }
        }

        public string DisplayedName
        {
            get { return displayedName; }
            set { displayedName = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }


        public string Identifier { get => identifier; set => identifier = value; }
        public string ConfigFolderPath { get => configFolderPath; set => configFolderPath = value; }
        public string ProductId { get => productId; set => productId = value; }
        public string AMZCategory { get => amzCategory; set => amzCategory = value; }
        public string PreName { get => preName; set => preName = value; }
    }
}
