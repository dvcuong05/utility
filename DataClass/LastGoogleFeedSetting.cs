﻿
namespace NCUtils.DataClass
{
    public class LastGoogleFeedSetting
    {
        string sheetID = "";
        string siteName = "";
        string host = "";
        string user = "";
        string pass = "";
        string dbName = "";
        string rage = "";

        public string SheetID { get => sheetID; set => sheetID = value; }
        public string SiteName { get => siteName; set => siteName = value; }
        public string Host { get => host; set => host = value; }
        public string User { get => user; set => user = value; }
        public string Pass { get => pass; set => pass = value; }
        public string DbName { get => dbName; set => dbName = value; }
        public string Rage { get => rage; set => rage = value; }
    }
}
