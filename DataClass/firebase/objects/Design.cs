﻿
using Newtonsoft.Json;

namespace NCUtils.DataClass.firebase.objects
{
    public class Design
    {
        string link;
        string name;

        public Design(string _link, string _name)
        {
            this.link = _link;
            this.name = _name;
        }

        [JsonProperty(PropertyName = "link")]
        public string DriveLink { get => link; set => link = value; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get => name; set => name = value; }
    }
}
