﻿

namespace NCUtils.DataClass.firebase
{
    public class FirebaseSetup
    {
        string firebaseUrl;
        string[] nodes;

        public string FirebaseUrl { get => firebaseUrl; set => firebaseUrl = value; }
        public string[] Nodes { get => nodes; set => nodes = value; }
    }
}
