﻿using System;
using System.Collections.Generic;

namespace NCUtils.DataClass
{
    public class TeescapeItemConfig
    {
        private bool isSingleUpload;
        private string tmpFullPath;
        private String imagePath;
        private String name;
        private String description;
        private bool isFinished;
        private bool isOnBack;
        private string mainkeyword;
        private String[] replaceKeywords;
        private List<string> tags = new List<string>();

        public String Description
        {
            get { return description; }
            set { description = value; }
        }

        public TeescapeItemConfig(String _imagePath, String _teeSpringName, String _description,  bool _isOnBack)
        {
            this.imagePath = _imagePath;
            this.name = _teeSpringName;
            this.description = _description;
            this.isOnBack = _isOnBack;
        }

        public TeescapeItemConfig()
        {
            // TODO: Complete member initialization
        }

        public bool IsOnBack
        {
            get { return isOnBack; }
            set { isOnBack = value; }
        }
        public String ImagePath
        {
            get { return imagePath; }
            set { imagePath = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        public bool IsFinished
        {
            get { return isFinished; }
            set { isFinished = value; }
        }
        public string TmpFullPath
        {
            get { return tmpFullPath; }
            set { tmpFullPath = value; }
        }
        public string[] ReplaceKeywords
        {
            get
            {
                return replaceKeywords;
            }

            set
            {
                replaceKeywords = value;
            }
        }

        public string Mainkeyword
        {
            get
            {
                return mainkeyword;
            }

            set
            {
                mainkeyword = value;
            }
        }

        public List<string> Tags
        {
            get
            {
                return tags;
            }

            set
            {
                tags = value;
            }
        }

        public bool IsSingleUpload
        {
            get
            {
                return isSingleUpload;
            }

            set
            {
                isSingleUpload = value;
            }
        }
    }
}
