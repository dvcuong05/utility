﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NCUtils.DataClass
{
  public  class TeescapeConfig
{
        private String folderPath;
        private String description;
        private Boolean isOnBack;
        private List<TeescapeStyle> stylesOfParent;
        private List<TeescapeItemConfig> teescapeItemsConfig;
        private String mainKeyword;
        private String[] replaceKeywords;
        private bool isSingleUpload;

        public String FolderPath
        {
            get { return folderPath; }
            set { folderPath = value; }
        }

        public String Description
        {
            get { return description; }
            set { description = value; }
        }

        public Boolean IsOnBack
        {
            get { return isOnBack; }
            set { isOnBack = value; }
        }

        public List<TeescapeStyle> StylesOfParent
        {
            get { return stylesOfParent; }
            set { stylesOfParent = value; }
        }

        public List<TeescapeItemConfig> TeescapeItemsConfig
        {
            get { return teescapeItemsConfig; }
            set { teescapeItemsConfig = value; }
        }

        public string[] ReplaceKeywords
        {
            get
            {
                return replaceKeywords;
            }

            set
            {
                replaceKeywords = value;
            }
        }

        public string MainKeyword
        {
            get
            {
                return mainKeyword;
            }

            set
            {
                mainKeyword = value;
            }
        }

        public bool IsSingleUpload
        {
            get
            {
                return isSingleUpload;
            }

            set
            {
                isSingleUpload = value;
            }
        }
    }
}
