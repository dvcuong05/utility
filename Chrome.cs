﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using System.Threading;
using System.IO;
using DataClass;

namespace Main.Controls
{
    public partial class Chrome : UserControl
    {
        public ChromiumWebBrowser browser;
        private FTPConfig config;
        private string csvOnServerPath;
        private string afterImport;
        private string afterImportFolderPath;
        private CancellationTokenSource cts;
        private string endpoint;

        public Chrome(string url, string chromeLocalData)
        {
            InitializeComponent();

            CefSettings settings = new CefSettings
            {
                PersistSessionCookies = true,
                IgnoreCertificateErrors = true,
                RemoteDebuggingPort = 8088,
                LogSeverity = LogSeverity.Error,
                CachePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\assets\\caches"
            };
            settings.RegisterScheme(new CefCustomScheme
            {
                SchemeName = LocalSchemeHandlerFactory.SchemeName,
                SchemeHandlerFactory = new LocalSchemeHandlerFactory()
            });
            if (!Cef.IsInitialized)
            {
                Cef.Initialize(settings);
            }
            
            browser = new ChromiumWebBrowser(url == null ? "" : url)
            {
                Dock = DockStyle.Fill
            };
        }

        public Chrome(string endpoint, FTPConfig config, string csvOnServerPath, string afterImport, string afterImportFolderPath, CancellationTokenSource cts)
        {
            InitializeComponent();
            this.config = config;
            this.csvOnServerPath = csvOnServerPath;
            this.afterImport = afterImport;
            this.cts = cts;
            this.afterImportFolderPath = afterImportFolderPath;
            this.endpoint = endpoint;
            CefSettings settings = new CefSettings
            {
                PersistSessionCookies = true,
                IgnoreCertificateErrors = true,
                RemoteDebuggingPort = 8088,
                LogSeverity = LogSeverity.Error,
                CachePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\assets\\caches"
            };
            settings.RegisterScheme(new CefCustomScheme
            {
                SchemeName = LocalSchemeHandlerFactory.SchemeName,
                SchemeHandlerFactory = new LocalSchemeHandlerFactory()
            });
            if (!Cef.IsInitialized)
            {
                Cef.Initialize(settings);
            }

            browser = new ChromiumWebBrowser("")
            {
                Dock = DockStyle.Fill
            };
        }
   
        private void Chrome_Load(object sender, EventArgs e)
        {
            browser.IsBrowserInitializedChanged += OnIsBrowserInitializedChanged;
            Controls.Add(browser);
        }

        private void OnIsBrowserInitializedChanged(object sender, EventArgs e)
        {
            //if (e.IsBrowserInitialized)
            {
                try
                {
                    runBrowser();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Read file has exception:" + ex);
                }
            }
        }

        public void setParams(FTPConfig config, string csvOnServerPath, string afterImport, CancellationTokenSource cts)
        {
            this.config = config;
            this.csvOnServerPath = csvOnServerPath;
            this.afterImport = afterImport;
            this.cts = cts;
        }

        // login
        public async void runBrowser()
        {
            await Task.Delay(2000);
            EventHandler<LoadingStateChangedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                //Wait for while page to finish loading not just the first frame
                if (!args.IsLoading)
                {
                    browser.LoadingStateChanged -= handler;
                    browser.GetSourceAsync().ContinueWith(async taskHtml =>
                    {
                        var html = taskHtml.Result;
                        if (html.Contains("done"))
                        {
                            await MoveCsvToDone(config, csvOnServerPath, afterImport, afterImportFolderPath);
                        }
                        else
                        {
                            Console.WriteLine("Can not import:  - " + csvOnServerPath + " - " + html);
                        }
                        Console.WriteLine("response ----:  - " + html);
                        closeForm();
                    });

                }
            };
            string p = "FILE=" + csvOnServerPath;
            string url = this.endpoint + "?" + p;
            browser.LoadingStateChanged += handler;
            browser.Load(url);

            try
            {
                String pathStr = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "/assets/debuger.txt";
                String isEnableStr = File.ReadAllText(pathStr);
                if (isEnableStr == "true")
                {
                    browser.ShowDevTools();
                }
            }
            catch (Exception) { }
        }

        public static async Task MoveCsvToDone(DataClass.FTPConfig config, string filePath, string newFilePath, string basePath)
        {

            if (config != null)
            {
                await DataClass.SFTPClient.MoveAsyn(config, filePath, newFilePath, basePath);
            }
            else
            {
                Console.WriteLine("IMPORTAN - MoveCsvToDone: " + filePath + " . Already import but move fail: ");
            }
        }

        private void closeForm()
        {
            browser.Invoke((MethodInvoker)delegate
            {
                Parent.Dispose();
            });
        }
    }
}
